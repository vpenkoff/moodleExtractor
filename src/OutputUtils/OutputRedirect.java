package OutputUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author viktor penkov
 */
public class OutputRedirect extends OutputStream{
    private final JTextArea textArea;
     
    public OutputRedirect(JTextArea textArea) {
        this.textArea = textArea;
    }
     
    @Override
    public void write(int b) throws IOException {
        // redirects data to the text area
        textArea.append(String.valueOf((char)b));
        // scrolls the text area to the end of data
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
    public static class SyncPrintOut {
        private static PrintStream printStream;
        
        public static void setPrintOut(JTextArea area) {
            try {
                printStream = new PrintStream(new OutputRedirect(area), true, Charset.forName("UTF-8").displayName());
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(OutputRedirect.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.setOut(printStream);
        }
        
        public static void printMsg(String message) {
            System.out.println(message);
        }
    }    
}

