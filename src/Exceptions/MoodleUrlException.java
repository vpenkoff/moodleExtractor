package Exceptions;

/**
 *
 * @author viktor penkov
 */
public class MoodleUrlException extends Exception {
    public MoodleUrlException() {super();}
    public MoodleUrlException(String message) { super(message); }
    public MoodleUrlException(String message, Throwable cause) {
         super(message, cause);
    }
    public MoodleUrlException(Throwable cause) { super (cause); }
}
