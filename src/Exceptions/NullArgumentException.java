package Exceptions;

/**
 *
 * @author viktor penkov
 */
public class NullArgumentException extends Exception{
    public NullArgumentException() {super();}
    public NullArgumentException(String message) { super(message); }
    public NullArgumentException(String message, Throwable cause) {
         super(message, cause);
    }
    public NullArgumentException(Throwable cause) { super (cause); }
}
