package Exceptions;

/**
 *
 * @author viktor penkov
 */
public class MoodleTokenException extends Exception{
    public MoodleTokenException() {super();}
    public MoodleTokenException(String message) { super(message); }
    public MoodleTokenException(String message, Throwable cause) {
         super(message, cause);
    }
    public MoodleTokenException(Throwable cause) { super (cause); }
}