package NetWrapper;

import Exceptions.MoodleTokenException;
import static Utils.ConvertData.ArrayFromJson;
import static Utils.ConvertData.ObjectFromJson;
import static Utils.ConvertData.checkResponseType;
import Utils.CoursesUtils;
import static OutputUtils.OutputRedirect.SyncPrintOut.printMsg;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author viktor penkov
 */
public abstract class Request {
    public static String getCourses(String url,
            String token) throws ProtocolException,
            IOException, FileNotFoundException, MoodleTokenException{
        
        HashMap<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/json");
        MoodleRequest request = new MoodleRequest(url, token,
        "core_course_get_courses", "options[ids][]", "json",
        headers, "POST");
        request.sendRequest();
        request.getResponse();
        request.getResponseGetCourses();
        return request.getRequestResponse();
    }
    
    public static String getCategories(String url,
            String token) throws ProtocolException,
            IOException, FileNotFoundException, MoodleTokenException {
        HashMap<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/json");
        MoodleRequest request = new MoodleRequest(url, token,
        "core_course_get_categories", "criteria[0][key]=" + 
                "&criteria[0][value]=", "json", headers, "POST");
        request.sendRequest();
        request.getResponse();
        request.getResponseGetCategories();
        return request.getRequestResponse();
    }
    
    public static String getCourse(String url,
            String token, String courseId) throws IOException, MoodleTokenException {
        HashMap<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        MoodleRequest request = new MoodleRequest(url, token,
        "core_course_get_contents", "courseid=" + courseId, "json", headers, "POST");
        request.sendRequest();
        request.getResponse();
        return request.getRequestResponse();
    }
    
    public static class MoodleRequest {
	private HttpURLConnection con;
	private final String token;
	private final String functionName;
	private final String functionParams;
	private final String responseFormat;
	private final String url;
        private String moodleUrl;
        private final String requestMethod;
        private final Map<String, String> requestHeaders;
        private Object requestResponse;

	public MoodleRequest(String url, String token,
                String functionName,
                String functionParams,
                String responseFormat,
                HashMap<String, String> requestHeaders,
                String requestMethod) {
            this.url = url;
            this.token = token;
            this.functionName = functionName;
            this.functionParams = functionParams;
            this.responseFormat = responseFormat;
            this.requestHeaders = requestHeaders;
            this.requestMethod = requestMethod;
        }
        
        private void buildMoodleUrl() {
            printMsg("Building the moodle url...");
            this.moodleUrl = url + "/webservice/rest/server.php" + 
                    "?wstoken=" + this.token + "&wsfunction=" + 
                    this.functionName + "&moodlewsrestformat=" + 
                    this.responseFormat;
            printMsg("Using " + this.moodleUrl + "...");
        }
        
        public void sendRequest() throws MalformedURLException, IOException {
            buildMoodleUrl();
            con = 
                    (HttpURLConnection) new URL(this.moodleUrl).openConnection();
            printMsg("Open connection to the moodle host...");
            con.setRequestMethod(this.requestMethod);
            printMsg("Setting request headers...");
            setConHeaders();
            con.setDoOutput(true);
            con.setUseCaches (false);
            con.setDoInput(true);
            DataOutputStream wr = new DataOutputStream (
                con.getOutputStream ());
            printMsg("Sending the request...");
            wr.writeBytes(this.functionParams);
            wr.flush ();
            wr.close ();
        }
        
        private void setConHeaders() {
            Iterator it = this.requestHeaders.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry<String, String>)it.next();
                this.con.setRequestProperty((String)pair.getKey(),
                        (String)pair.getValue());
            }
        }
        
        public void getResponse() throws IOException, MoodleTokenException {
            printMsg("Receiving response...");
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            checkResponseForErrors(response.toString());
            this.requestResponse = response;
            rd.close();
        }
        
        public void getResponseGetCourses() {
            String jsonType = checkResponseType(this.requestResponse.toString());
            switch (jsonType) {
                case "array":
                    System.out.printf("Available Courses: %d\n",
                            Utils.CoursesUtils.
                                    courseCounter(ArrayFromJson(this.requestResponse.toString())));
                    break;
                case "object":
                    printMsg(ObjectFromJson(this.requestResponse.toString()).toString());
                    break;
            }
        }
        
        public void getResponseGetCategories() {
            System.out.printf("Available Categories: %d\n",
                CoursesUtils.categoriesCounter(ArrayFromJson(this.
                        requestResponse.toString())));
        }

        public String getRequestResponse() {
            return this.requestResponse.toString();
        }
        
        public void checkResponseForErrors(String response) throws MoodleTokenException {
            String cause = null;
            String jsonType = checkResponseType(response);
            if (jsonType.equals("object")) {
                JSONObject obj = (JSONObject) JSONValue.parse(response);
                if (obj.containsKey("exception")) {
                    Iterator it = obj.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        if (pair.getKey().equals("message"))
                            cause = (String) pair.getValue();
                    }
                }
                throw new MoodleTokenException("Moodle host responds with: " + cause);
            } 
                /* guess that it is an array, containing needed json data, so
                    continue execution
                */
        }
    }
}