package Utils;

/**
 *
 * @author viktor penkov
 */
public abstract class SystemUtils {
    private static final double req_version = 1.8;
    
    public static boolean checkJREversion() {
        String version = System.getProperty("java.version");
        String number = version.substring(0, 3);
        Double convertedNum = Double.parseDouble(number);
        if (convertedNum < req_version)
            return false;
        return true;
    }
}
