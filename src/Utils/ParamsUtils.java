package Utils;
import java.util.regex.*;
import Exceptions.MoodleUrlException;
import Exceptions.NullArgumentException;

/**
 *
 * @author viktor penkov
 */
public class ParamsUtils {
    public static void validateUrl(String url) throws NullArgumentException,
            MoodleUrlException {
        if (url == null || url.isEmpty())
            throw new NullArgumentException("Url parameter missing");
        
        Pattern pattern = Pattern.compile("((https?):((//)|(\\\\\\\\))+"+
                "[\\\\w\\\\d:#@%/;$()~_?\\\\+-=\\\\\\\\\\\\.&]*)");
        Matcher matcher = pattern.matcher(url);
        if (!(matcher.find()))
            throw new MoodleUrlException("Invalid url");
    }
    
    public static void checkToken(String token) throws NullArgumentException {
        if ((token == null) || token.isEmpty())
            throw new NullArgumentException("Token param is empty");
    }
}
