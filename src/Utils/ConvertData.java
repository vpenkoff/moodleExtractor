package Utils;
import static OutputUtils.OutputRedirect.SyncPrintOut.printMsg;
import com.ibm.icu.text.Transliterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author viktor penkov
 */

public abstract class ConvertData {
        public static String checkResponseType(String response) {
            String type = null;
            Object obj = JSONValue.parse(response);
            if (obj instanceof JSONArray) {
                type = "array";
            } else if (obj instanceof JSONObject) {
                type = "object";
            }
            return type;
        }
        
	public static JSONArray ArrayFromJson(String response){
            JSONParser parser = new JSONParser();
	    JSONArray data = null;
	    try
	    {
	      data = (JSONArray) parser.parse(response);
	    } catch (ParseException ex) { 
                printMsg(ex.getMessage());
                Logger.getLogger(ConvertData.class.getName()).log(Level.SEVERE,
                    null, ex);
            }
            return data;
	}
        
        public static JSONObject ObjectFromJson(String response) {
            JSONParser parser = new JSONParser();
            JSONObject data = null;
            try {
                data = (JSONObject) parser.parse(response);
            } catch (ParseException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(ConvertData.class.getName()).log(Level.SEVERE,
                    null, ex);
            }
            return data;
        }
        
        public static String convertStringToUTF8(String text) {
            String encoded_string = null;
            try {
                byte[] raw_bytes = text.getBytes("UTF8");
                encoded_string = new String(raw_bytes, "UTF8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ConvertData.class.getName()).log(Level.SEVERE, null, ex);
            }
            return encoded_string;
        }
        
        public static String cyrToLat(String text) {
            //String id = "Any-Latin; NFD;";
            String id = "Bulgarian-Latin/BGN; NFD; [:Nonspacing Mark:] Remove; NFC;";
            String latin = Transliterator.getInstance(id).transform(text);
            return latin;
        }
}