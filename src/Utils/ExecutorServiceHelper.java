package Utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author viktor penkov
 */
public class ExecutorServiceHelper {
    private static ExecutorService execService;

        public ExecutorServiceHelper() {
            execService = Executors.newFixedThreadPool(4);
        }
        
        public void execTask(Runnable task) {
            execService.execute(task);
        }
        
        public void shutdownExecService() {
            execService.shutdown();
        }
        
        public boolean isExecServiceDown() {
            return execService.isShutdown();
        }
        
        public boolean execServiceAwaitTermination() throws InterruptedException {
            return execService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        }
        
        public boolean isExecServiceTerminated() {
            return execService.isTerminated();
        }
}
