package Utils;

import Exceptions.MoodleTokenException;
import static Utils.FileUtils.checkIfFolderExists;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import SwingComponents.NextWindow;
import static OutputUtils.OutputRedirect.SyncPrintOut.printMsg;

/**
 *
 * @author viktor penkov
 */
public abstract class ThreadUtils {
    private static final String SRC_PATH = "moodleContent";
    private static final String ZIP_FILE = "moodleContent.zip";

    /* 
    Use Singleton Design Pattern to get only one instance of the
    Semaphore object.
    The semaphore is used to ensure that the downloading of the moodle content
    is not interrupted and the zip utility is used AFTER the downloading
    */

    public static class SingleSemaphore {
        private static Semaphore semaphore;
        private static final SingleSemaphore instance = new SingleSemaphore();
        
        private SingleSemaphore() {
            semaphore = new Semaphore(1);
        }
        
        public Semaphore getSemaphore() {
            return semaphore;
        }
        
        public static SingleSemaphore getInstance(){
            return instance;
        }
    }
    
    public static void runNodeOps(ExecutorServiceHelper execService,
            DefaultMutableTreeNode node) throws MoodleTokenException {
        try {
            SingleSemaphore.getInstance().getSemaphore().acquire();
        } catch (InterruptedException ex) {
            printMsg(ex.getMessage());
            Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        NextWindow.execService = new ExecutorServiceHelper();
        try {
            CoursesUtils.traverseTreeNodes(node);
        } catch (InterruptedException ex) {
            printMsg(ex.getMessage());
            Logger.getLogger(NextWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        NextWindow.execService.shutdownExecService();
        if (!NextWindow.execService.isExecServiceTerminated()) {
            try {
                if (!NextWindow.execService.execServiceAwaitTermination())
                    printMsg("ERROR Occured when trying to finish all theads!");
            } catch (InterruptedException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(NextWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        SingleSemaphore.getInstance().getSemaphore().release();
    }
    
    public static void runZip(String dwPath) {
        String srcPath = dwPath.concat(File.separator + SRC_PATH);
        String zipFile = dwPath.concat(File.separator + ZIP_FILE);
        FileUtils.ZipObj zipobj = new FileUtils.ZipObj(zipFile, srcPath);
         try {
            SingleSemaphore.getInstance().getSemaphore().acquire();
        } catch (InterruptedException ex) {
            printMsg(ex.getMessage());
            Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        zipobj.compressContent();
        SingleSemaphore.getInstance().getSemaphore().release();
    }
    
    public static void runRemove() {
        try {
            SingleSemaphore.getInstance().getSemaphore().acquire();
            FileUtils.removeFile(FileUtils.ZipObj.getMoodleSourceFolder());
        } catch (IOException ex) {
            printMsg(ex.getMessage());
            Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        SingleSemaphore.getInstance().getSemaphore().release();
    }
    
    public static void runJobs(ExecutorServiceHelper execService,
            DefaultMutableTreeNode node, String dwPath) throws MoodleTokenException, Exception {
        int val = checkIfFolderExists(dwPath.concat(File.separator + SRC_PATH));
        switch (val) {
            case -1:
            case 0:
                runNodeOps(execService, node);
                runZip(dwPath);
                runRemove();
                break;
            case 1:
                break;
            default:
                break;
        }
    }
}
