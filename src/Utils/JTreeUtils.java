package Utils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import javax.swing.tree.DefaultMutableTreeNode;
import SwingComponents.MainWindow;

/**
 *
 * @author viktor penkov
 */
public abstract class JTreeUtils {
    public static void createJTreeEntries(DefaultMutableTreeNode top,
            ArrayList<String> courseCategories) {
        DefaultMutableTreeNode category;
        Iterator<String> it = courseCategories.iterator();
        while(it.hasNext()) {
            Object el = it.next();
            String name = (String)el;
            category = new DefaultMutableTreeNode(name);
            top.add(category);
        }
    }
    
    public static void createJTreeEntriesCategory(DefaultMutableTreeNode top, 
            ArrayList<Object> categories) {
        for (Object catObj : categories) {
            HashMap<Long, String> cat_el = (HashMap<Long, String>) catObj;
            Iterator it = cat_el.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry c_pair = (Map.Entry)it.next();
                String catName = (String)c_pair.getValue();
                DefaultMutableTreeNode cat_node = new DefaultMutableTreeNode(catName);
                top.add(cat_node);
            }
        }
    }

    public static void createJTreeEntriesCourses(DefaultMutableTreeNode top,
            ArrayList<Object> courses, ArrayList<Object> categories) {
        Enumeration catChildren = top.children();
        while ( catChildren.hasMoreElements()) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)catChildren.nextElement();
            String childName = (String)child.getUserObject();
            for (Object catObj : categories) {
                HashMap<Long, String> cat_el = (HashMap<Long, String>) catObj;
                Iterator cat_it = cat_el.entrySet().iterator();
                while (cat_it.hasNext()) {
                    Map.Entry cat_pair = (Map.Entry)cat_it.next();
                    String catName = (String)cat_pair.getValue();
                    Long catId = (Long)cat_pair.getKey();
                    if (childName.equals(catName)){
                        for (Object c : courses) {
                            HashMap<Long, String> c_el = (HashMap<Long, String>) c;
                            Iterator it = c_el.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry c_pair = (Map.Entry)it.next();
                                String courseName = (String)c_pair.getValue();
                                Long course_catId = (Long)c_pair.getKey();
                                if (Objects.equals(course_catId, catId)) {
                                    DefaultMutableTreeNode childCourse = new DefaultMutableTreeNode(courseName);
                                    child.add(childCourse);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void createJTreeEntriesFull(DefaultMutableTreeNode top,
            HashMap<Long, String> categories, HashMap<Long, String> courses) {
        Iterator cats = categories.entrySet().iterator();
        Iterator course = courses.entrySet().iterator();
        
        while(cats.hasNext()) {
            Map.Entry pair = (Map.Entry)cats.next();
            String name = (String) pair.getValue();
            Long id = (Long) pair.getKey();
            DefaultMutableTreeNode cat_node = new DefaultMutableTreeNode(name);
            while(course.hasNext()) {
                Map.Entry course_pair = (Map.Entry)course.next();
                String course_name = (String) course_pair.getValue();
                Long course_cat_id = (Long) course_pair.getKey();
                if (Objects.equals(course_cat_id, id)) {
                    DefaultMutableTreeNode course_node = new DefaultMutableTreeNode(course_name);
                    cat_node.add(course_node);
                }
            top.add(cat_node);
            }
        }
        
    }
    
    public static DefaultMutableTreeNode jtreeConfig() {
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Categories");
        String courses = MainWindow.getCourses();
        String categories = MainWindow.getCategories();
        ArrayList<Object> categories_and_ids = CoursesUtils.createCourseCategories(categories);
        ArrayList<Object> courses_and_catIds = CoursesUtils.getCourseCategories(courses);
        createJTreeEntriesCategory(top, categories_and_ids);
        createJTreeEntriesCourses(top, courses_and_catIds, categories_and_ids);
        return top;
    }
}
