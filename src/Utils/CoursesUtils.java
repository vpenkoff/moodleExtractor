package Utils;
import Exceptions.MoodleTokenException;
import NetWrapper.Request;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import SwingComponents.FileChooseDialog;
import SwingComponents.MainWindow;
import SwingComponents.NextWindow;
import static OutputUtils.OutputRedirect.SyncPrintOut.printMsg;
import static Utils.ConvertData.convertStringToUTF8;
import static Utils.ConvertData.cyrToLat;
import org.json.simple.*;
/**
 *
 * @author viktor penkov
 */
public abstract class CoursesUtils {
    public static int courseCounter(JSONArray courses) {
        return courses.size();
    }
    
    public static int categoriesCounter(JSONArray categories) {
        return categories.size();
    }
    
    public static ArrayList<Object> createCourseCategories(String data) {
        ArrayList<Object> all_categories = new ArrayList();
        Object obj = JSONValue.parse(data);
        if (obj instanceof JSONArray) {
            JSONArray arr = (JSONArray)obj;
            Iterator it = arr.iterator();
            while (it.hasNext()) {
                HashMap<Long, String> categories = new HashMap();
                Object element = it.next();
                JSONObject el = (JSONObject) element;
                categories.put((Long)el.get("id"),
                        (String)el.get("name"));
                all_categories.add(categories);
            }
        }
        return all_categories;
    }
    
    public static ArrayList<String> getCategoriesOnly(
            HashMap<Long, String> ids_categories) {
        ArrayList<String> categories = new ArrayList();
        Iterator hashmap_it = ids_categories.entrySet().iterator();
        while (hashmap_it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)hashmap_it.next();
            categories.add((String)pair.getValue());
        }
        return categories;
    }
    
    public static ArrayList<Object> getCourseCategories(String data) {
        ArrayList<Object> courses = new ArrayList();
        Object obj = JSONValue.parse(data);
        if (obj instanceof JSONArray) {
            JSONArray arr = (JSONArray)obj;
            Iterator it = arr.iterator();
            while (it.hasNext()) {
                Object element = it.next();
                JSONObject el = (JSONObject) element;
                HashMap<Long, String> courses_and_catId = new HashMap();
                courses_and_catId.put((Long)el.get("categoryid"),
                        (String)el.get("fullname"));
                courses.add(courses_and_catId);
            }
        }
        return courses;
    }
    
    public static String getCourseId(String data,
            String name) {
        String course_id = null;
        Object obj = JSONValue.parse(data);
        if (obj instanceof JSONArray) {
            JSONArray arr = (JSONArray)obj;
            Iterator it = arr.iterator();
            while (it.hasNext()) {
                Object element = it.next();
                JSONObject el = (JSONObject) element;
                if (name.equals((String) el.get("fullname"))) {
                    course_id = String.valueOf(el.get("id"));
                }
            }
        }
        return course_id;
    }

    public static HashMap<String, HashMap<String, String>> getCourseFilesAndPages(JSONArray contents) {
        HashMap<String, HashMap<String, String>> type_resources = new HashMap();
        Iterator contentsIt = contents.iterator();
        while (contentsIt.hasNext()) {
            Object contentObj = contentsIt.next();
            JSONObject content = (JSONObject) contentObj;
            if (content.get("type").equals("file")) {
                Object filename = content.get("filename");
                Object fileurl = content.get("fileurl");
                HashMap<String, String> resources = new HashMap();
                resources.put((String)filename, (String)fileurl);
                type_resources.put("file", resources);
            }
        }
        if (!type_resources.isEmpty())
            return type_resources;
        return null;
    }
    
    public static HashMap<String, HashMap<String, String>> getCourseFolders(JSONArray contents) {
        HashMap<String, HashMap<String, String>> type_resources = new HashMap();
        Iterator contentsIt = contents.iterator();
        while (contentsIt.hasNext()) {
            Object contentObj = contentsIt.next();
            JSONObject content = (JSONObject) contentObj;
            if (content.get("type").equals("file")) {
                Object filepath = content.get("filepath");
                Object filename = content.get("filename");
                Object fileurl = content.get("fileurl");
                HashMap<String, String> resources = new HashMap();
                resources.put((String)filename, (String)fileurl);
                type_resources.put((String)filepath, resources);
            }
        }
        if (!type_resources.isEmpty())
            return type_resources;
        return null;
    }

    public static HashMap<String, HashMap<String, String>> getCourseLabels(JSONObject module) {
        HashMap<String, String> resources = new HashMap();
        HashMap<String, HashMap<String, String>> type_resources = new HashMap();
        String cyr_name = (String)module.get("name");
        String cyr_desc = (String)module.get("description");
        String lat_name = cyrToLat(cyr_name);
        String lat_desc = cyrToLat(cyr_desc);
        resources.put(lat_name, lat_desc);
        //resources.put((String)module.get("name"), (String)module.get("description"));
        type_resources.put("label", resources);
        if (!type_resources.isEmpty())
            return type_resources;
        return null;
    }
    
    public static HashMap<String, HashMap<String, String>> getCourseUrls(JSONArray contents) {
        HashMap<String, HashMap<String, String>> type_resources = new HashMap();
        Iterator contentsIt = contents.iterator();
        while (contentsIt.hasNext()) {
            Object contentObj = contentsIt.next();
            JSONObject content = (JSONObject) contentObj;
            if (content.get("type").equals("url")) {
                Object filename = content.get("filename");
                Object fileurl = content.get("fileurl");
                HashMap<String, String> resources = new HashMap();
                resources.put((String)filename, (String)fileurl);
                type_resources.put("url", resources);
            }
        }
        if (!type_resources.isEmpty())
            return type_resources;
        return null;
    }
    
    public static void getCourseContents(String data,
            String courseName, String catName,
            String downloadPath, String token) 
            throws MalformedURLException {
        ArrayList<Object> resources = new ArrayList();
        HashMap<String, HashMap<String, String>> labels;
        HashMap<String, HashMap<String, String>> filesAndPages;
        HashMap<String, HashMap<String, String>> urls;
        Object obj = JSONValue.parse(data);
        if (obj instanceof JSONArray) {
            JSONArray arr = (JSONArray) obj;
            Iterator it = arr.iterator();
            while (it.hasNext()) {
                Object element = it.next();
                JSONObject contentEl = (JSONObject) element;
                JSONArray modules = (JSONArray)contentEl.get("modules");
                Iterator modIt = modules.iterator();
                while (modIt.hasNext()) {
                    Object moduleObj = modIt.next();
                    JSONObject module = (JSONObject) moduleObj;
                    if (module.get("modname").equals("label")) {
                        labels = getCourseLabels(module);
                        if (labels != null && !(resources.contains(labels)))
                            resources.add(labels);
                        Thread t = new Thread(new ThreadLabelDownload(labels,
                                courseName, catName, downloadPath));
                        NextWindow.execService.execTask(t);
                        
                    }
                    if (module.get("modname").equals("resource")) {
                        JSONArray contents = (JSONArray)module.get("contents");
                        filesAndPages = getCourseFilesAndPages(contents);
                        if (filesAndPages != null && !(resources.contains(filesAndPages)))
                            resources.add(filesAndPages);
                        Thread t = new Thread(new ThreadFilesDownload(filesAndPages,
                                courseName, catName, downloadPath, token));                        
                        NextWindow.execService.execTask(t);
                    }
                    if (module.get("modname").equals("page")) {
                        JSONArray contents = (JSONArray)module.get("contents");
                        filesAndPages = getCourseFilesAndPages(contents);
                        if (filesAndPages != null && !(resources.contains(filesAndPages)))
                            resources.add(filesAndPages);
                        Thread t = new Thread(new ThreadFilesDownload(filesAndPages,
                                courseName, catName, downloadPath, token));
                        NextWindow.execService.execTask(t);
                    }
                    if (module.get("modname").equals("url")) {
                        JSONArray contents = (JSONArray)module.get("contents");
                        urls = getCourseUrls(contents);
                        if (urls != null && !(resources.contains(urls)))
                            resources.add(urls);
                        Thread t = new Thread(new ThreadURLDownload(urls,
                                courseName, catName, downloadPath));
                        NextWindow.execService.execTask(t);
                    }
                    if (module.get("modname").equals("folder")) {
                        JSONArray contents = (JSONArray)module.get("contents");
                        String rootPath = (String) module.get("name");
                        filesAndPages = getCourseFolders(contents);
                        HashMap<Object, Object> directories = new HashMap();
                        directories.put(rootPath, filesAndPages);
                        if (filesAndPages != null && !(resources.contains(filesAndPages)))
                            resources.add(directories);
                        Thread t = new Thread(new ThreadDirectoriesDownload(directories,
                                courseName, catName, downloadPath, token));
                        NextWindow.execService.execTask(t);
                    }
                }
            }
        }
        //if (!resources.isEmpty())
        //    return resources;
        
        //return null;
    }
    
    private static class ThreadLabelDownload implements Runnable {
        
        HashMap<String, HashMap<String, String>> labels;
        String courseName;
        String catname;
        String downloadPath;
        public ThreadLabelDownload(Object labels, Object courseName, Object catName, Object dwPath) {
            this.labels = (HashMap<String, HashMap<String, String>>)labels;
            this.courseName = (String)courseName;
            this.catname = (String)catName;
            this.downloadPath = (String)dwPath;
        }
        
        @Override
        public void run(){
            try {
                FileUtils.downloadLabels(labels, courseName, catname, downloadPath);
            } catch (IOException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(CoursesUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static class ThreadURLDownload implements Runnable {
        
        HashMap<String, HashMap<String, String>> urls;
        String courseName;
        String catname;
        String downloadPath;
        public ThreadURLDownload(Object urls, Object courseName, Object catName, Object dwPath) {
            this.urls = (HashMap<String, HashMap<String, String>>)urls;
            this.courseName = (String)courseName;
            this.catname = (String)catName;
            this.downloadPath = (String)dwPath;
        }
        
        @Override
        public void run(){
            try {
                FileUtils.downloadURLS(urls, courseName, catname, downloadPath);
            } catch (IOException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(CoursesUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static class ThreadFilesDownload implements Runnable {
        
        HashMap<String, HashMap<String, String>> resources;
        String courseName;
        String catname;
        String downloadPath;
        String token;
        public ThreadFilesDownload(Object resources,
                Object courseName, Object catName,
                Object dwPath, Object token) {
            this.resources = (HashMap<String, HashMap<String, String>>)resources;
            this.courseName = (String)courseName;
            this.catname = (String)catName;
            this.downloadPath = (String)dwPath;
            this.token = (String)token;
        }
        
        @Override
        public void run(){
            try {
                FileUtils.downloadFileOrPage(resources,
                        courseName, catname, downloadPath, token);
            } catch (IOException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(CoursesUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static class ThreadDirectoriesDownload implements Runnable {
        
        HashMap<Object, Object> dirs;
        String courseName;
        String catname;
        String downloadPath;
        String token;
        public ThreadDirectoriesDownload(Object dirs, Object courseName,
                Object catName, Object dwPath, Object token) {
            this.dirs = (HashMap<Object, Object>)dirs;
            this.courseName = (String)courseName;
            this.catname = (String)catName;
            this.downloadPath = (String)dwPath;
            this.token = (String)token;
        }
        
        @Override
        public void run(){
            try {
                FileUtils.downloadFolders(dirs, courseName, catname, downloadPath, token);
            } catch (IOException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(CoursesUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void traverseTreeNodes(DefaultMutableTreeNode node) throws InterruptedException, MoodleTokenException {
        
        String url = MainWindow.getUrl();
        String token = MainWindow.getToken();
        String courses = MainWindow.getCourses();
        
        String raw_response = null;
        String response = null;
        
        if (node.isLeaf()) {
            //printMsg(convertStringToUTF8(node.toString()));
            //String courseName = convertStringToUTF8(node.toString());
            String courseName = cyrToLat(node.toString());
            String nodeParent = node.getParent().toString();
            String id = CoursesUtils.getCourseId(courses, node.toString());
            try {
                raw_response = Request.getCourse(url, token, id);
                response = convertStringToUTF8(raw_response);
            } catch (IOException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(NextWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
            //printMsg(response);
            try {
                CoursesUtils.getCourseContents(response, courseName, nodeParent, FileChooseDialog.getDirPath(), token);
                //printMsg(contents.toString());
            } catch (MalformedURLException ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(NextWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                printMsg(ex.getMessage());
                Logger.getLogger(NextWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Enumeration children = node.children();
            if (children != null) {
                while (children.hasMoreElements()) {
                    traverseTreeNodes((DefaultMutableTreeNode) children.nextElement());
                }
            }
        }
    }
}
