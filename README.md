# moodleExtractor
moodleExtractor - software to download Moodle course and category contents 


The project represents a software client application, which connects to a remote server, that runs the Moodle platform,  and retrieves  available course content resources. The application can download the following types of  resources: File, Folder, Label, Page, Url. The user is able to select and download resources within specific course, whole category or all available courses within categories. After successfully downloaded content, it is put in a universal ZIP format, which can be used for easily redistribution and organisation. 
To prevent security issues, a user token is needed for successfully connection to the Moodle server. The token is given by the administrator of the Moodle platform.

To compile:
clone the repo and execute the following steps:
ant compile
ant package

After that, a jar in the build directory will be present.
